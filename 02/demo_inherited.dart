import 'car/car.dart';
import 'car/truck.dart';

void main() {
  Car car = Car(name: 'Car 1');
  print(car);

  car.carEvent = () {
    print('OK');
    // Phần thực thi nằm ở main
  };
  car.sayHelo();
  // car.seat = 4;
  print(car.seat);
  car.sayHi();

  Truck truck = Truck(name: 'Xe Ben', wheel: 20);
  truck.id = 12;
  print('''${truck.id} - ${truck.wheel} - ${truck.name} - ${truck.yearOf}''');
  truck.sayHi();
}
