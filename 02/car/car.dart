import 'base_cart.dart';

class Car extends BaseCar {
  //extends Sẽ kế thừa class trước đó.
  int _id = 0;
  String name;
  int yearOf;
  Function carEvent = () => {print("Haha...!")};

  Car({required this.name, this.yearOf = 2020});
  void doSomething() {
    print('do something');
  }

  @override
  String toString() {
    // TODO: implement toString
    return '''Carname: ${this.name} - year: $yearOf''';
  }

  @override
  // TODO: implement id
  int get id => this._id;

  @override
  void sayHelo() {
    print('Xin chào, $name');

    // if (this.carEvent != null) {
    //   this.carEvent!();
    // }
    // Gọi trong đây

    this.carEvent();
  }

  @override
  void sayHi() {
    // TODO: implement sayHi
    print('Hi......!');
  }

  @override
  set id(int id) {
    // TODO: implement id
    this._id = id;
  }
}
