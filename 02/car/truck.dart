import 'car.dart';

class Truck extends Car {
  int wheel;
  Truck({required name, this.wheel = 16}) : super(name: name);

  /// Không cần override nữa vì Car đã override rồi.
  @override
  void sayHi() {
    // TODO: implement sayHi
    print('TO to....');
  }
}
