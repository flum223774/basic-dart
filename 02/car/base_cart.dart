abstract class BaseCar {
  // Đối tượng này có những đặc điểm chung mà bắt buộc các đối tượng kế thừa phải có.
  // Nó định hình cấu trúc của đối tượng kế thừa
  // Nên dùng để định nghĩa các interface => các method
  int get id;
  set id(int id);
  void sayHelo();

  void sayHi() {
    print('Hi!');
  }

  int? seat;
}
