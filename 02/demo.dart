abstract class BaseCarDemo {
  void getWheel();
  void sayHello() {
    print('Hello');
  }
}

class CarDemo extends BaseCarDemo {
  String name;
  int? year;
  CarDemo({required this.name, this.year = 2000});

  @override
  void getWheel() {
    // TODO: implement getWheel
    print('4');
  }
}

class Rangger extends CarDemo {
  String? color;
  Rangger(String rangerName, {int? rangerYear, this.color})
      : super(name: rangerName, year: rangerYear);
}

main() {
  CarDemo car = CarDemo(name: 'Rangger');
  car.getWheel();
  Rangger rangger = Rangger("Rangrover", color: 'Red');
  print(rangger.name);
  rangger.sayHello();
  print(rangger.year);
  print(rangger.color);
}
