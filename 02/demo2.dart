class A {
  void sayHello() {
    print('Hello');
  }

  String? name;
}

class B {
  void sayHi() {
    print('Hello');
  }
}

class C implements A, B {
  @override
  void sayHello() {
    // TODO: implement sayHello
  }

  @override
  void sayHi() {
    // TODO: implement sayHi
  }

  @override
  String? name;
}

class D with A implements B, C {
  @override
  void sayHi() {
    // TODO: implement sayHi
  }
}
