import 'demo_inherited2.dart';

class MixA {
  int addition(int a, int b) {
    return a + b;
  }
}

class MixB {
  DateTime time = DateTime.now();
  int subtraction(int a, int b) {
    return a - b;
  }

  get currentTime => time;
}

/// Không thể mixin 1 class đang kế thừa 1 class khác.
/// Không thể kế thừa 1 class có contructor
/// Không thể kế thừa 1c abstract class
/// Nó chỉ kế thừa class chứa các method.
class C with MixA, MixB implements A {
  @override
  String name;

  C({required this.name});

  @override
  void sayHi() {
    // TODO: implement sayHi
  }

  @override
  // TODO: implement saySomething
  get saySomething => throw UnimplementedError();
}

main() {
  C c = C(name: 'C Name');
  print(c.addition(1, 2));
  print(c.subtraction(2, 1));
  print(c.currentTime);
}
