class A {
  void sayHi() {
    print('Hi...!');
  }

  get saySomething => 'Something...!';

  String name;
  A(this.name);
}

class B {
  String name;
  B(this.name);
  void sayHello() {
    print('Hello...!');
  }
}

/// Kế thừa interface => Ghi đè tất cả
class C implements A, B {
  @override
  void sayHello() {
    // TODO: implement sayHello
    print('C Hello');
  }

  @override
  void sayHi() {
    // TODO: implement sayHi
    print('C Hi');
  }

  @override
  // TODO: implement saySomething
  get saySomething => 'C say something';

  @override
  String name;
  C(this.name);
  @override
  String toString() {
    // TODO: implement toString
    return 'C: ${name}';
  }
}

main() {
  C c = C('Name');
  c.sayHi();
  c.sayHello();
  print(c.saySomething);
  print(c.toString());
}
