void main() {
  Sharp sharp = Sharp(canh: 2);
  print(sharp.hasNote);
  print(sharp);
  sharp.printNote();
}

/// CLASS
abstract class BaseShap {
  bool get hasNote;
}

class MixinSharp {
  String checkCanh(int soCanh) {
    switch (soCanh) {
      case 3:
        return 'Tam giác';
      case 4:
        return 'Tứ giác';

      case 5:
        return 'Ngũ giác';
      default:
        return 'Số cạnh nhập vào không hợp lệ. \nVui lòng nhập lại';
    }
  }
}

class SharpRequire {
  void printNote() {
    print('Your note');
  }
}

class Sharp extends BaseShap with MixinSharp implements SharpRequire {
  int canh;
  String? note;
  Sharp({this.canh = 3, this.note});

  @override
  String toString() {
    return this.checkCanh(this.canh);
  }

  @override
  // TODO: implement hasNote
  bool get hasNote => this.note != null && this.note!.isNotEmpty;

  @override
  void printNote() {
    print(this.note ?? '''Don't have note''');
  }
}
