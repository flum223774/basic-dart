import 'dart:async';
/* 
  Async và await => Bất đồng bộ
  - Lấy dữ liệu api qua mạng
  - Lưu dữ liệu xuống database
  - Đọc dữ liệu từ file.
  https://dart.dev/codelabs/async-await

  Dart là ngôn ngữ single-thread, những vẫn đc hỗ trợ tính năng bất đồng bộ bằng các Isolate 
  ----------------------------------------
  Tài liệu: https://medium.com/dartlang/dart-asynchronous-programming-isolates-and-event-loops-bffc3e296a6a
  Isolate là gì?  It’s like a little space on the machine with its own, 
  private chunk of memory and a single thread running an event loop. 
  Là một khoảng trống nhỏ trong máy, với bộ nhớ và riêng và chạy 1 luồng theo vòng lặp sự kiện
  ----------------------------------------
  


 */

Future<void> printOrderMessage() async {
  try {
    var order = await fetchUserOrder();
    print('Awaiting user order...');
    print(order);
  } catch (err) {
    print('Caught error: $err');
  }
}

Future<String> fetchUserOrder() {
  // Imagine that this function is more complex.
  var str = Future.delayed(
      Duration(seconds: 4), () => throw 'Cannot locate user order');
  return str;
}

// main() {
//   printOrderMessage();
//   print('Fetching user order...');
// }

// Future main() async {
//   await printOrderMessage();
//   print('Fetching user order...');
// }

main() {
  fetchUserOrder()
      .then((value) => print('Fetch ok and dont have error.'))
      .catchError((onError) => {print('Error is: $onError')});
  print('Fetching user order...');
}
