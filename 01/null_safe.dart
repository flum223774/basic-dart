void main() {
  // Nullable variable
  List<int> arr = [1, 2, 3, 4];

  // Item in list can be null
  List<int?> arr2 = [1, 2, 3, 4, null];

  // List can be null
  List<int>? arr3 = null;
  // List and item in list can be null
  List<int?>? arr4 = null;

  int? aNullableInt = arr[1];
  int number = arr2[1]!;
  int? number5 = arr2[4];
  print('$aNullableInt, $number, $number5');
  // late keyword
  late int number_index;
  number_index = arr2[1] ?? 1;
  // ?? operator help check variable null

  Map<String, int> myMap = <String, int>{'one': 1};
  //  Map type value maybe can null => check null state
  int uhOh = myMap['one'] ?? 0;
  print(uhOh);
}
