class Car {
  String _type = '';
  String? color, brand;
  String call() {
    return "Method call Car";
  }

  Car({this.brand, this.color});

  Car.contructDemo({this.color});
  set typeCart(String type) {
    this._type = type;
  }

  String get typeCart => _type;

  factory Car.fromJson(Map<String, dynamic> json) {
    return Car(brand: json['brand'], color: json['color']);
  }
  Car.create() {
    this._getCar();
    this.brand = "Brand 4";
    this.color = "Color 4";
  }
  void _getCar() {
    print("Call a function when init class");
  }

  @override
  String toString() {
    // TODO: implement toString
    return "This is Car class";
  }
}
