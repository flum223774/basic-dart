void main() {
  var storeData = [
    {"id": 1, "name": "Nike", "distance": 34},
    {"id": 2, "name": "Adidas", "distance": 16},
    {"id": 3, "name": null, "distance": 93},
    {"id": 4, "name": "Jyan", "distance": 78}
  ];

  List<Store> storeList = <Store>[];

  // Parste json to list
  for (var data in storeData) {
    Store store = Store.fromJson(data);
    storeList.add(store);
  }

  // Add store new to list
  var store = Store(5, "Levil", 59);
  storeList.add(store);

  print('Store total: ' + storeList.length.toString());

  // tìm Store có khoảng cách ngắn nhất
  Store? storeMin = minDistance(storeList);
  if (storeMin == null)
    print('Not found Store');
  else {
    String nameStore = storeMin.name ?? '[Empty]';
    int distanceStore = storeMin.distance ?? 0;
    print('Minimum Store distance: $nameStore (with distance: $distanceStore)');
  }
}

/// Find Minimum Store distance
Store? minDistance(List<Store> storeList) {
  Store? storeMin;
  for (var store in storeList) {
    if (storeMin == null) {
      storeMin = store;
      continue;
    }

    if ((storeMin.distance ?? 0) > (store.distance ?? 0)) storeMin = store;
  }

  return storeMin;
}

class Store {
  int?
      id; // Nếu để int thì bị lỗi tại function 'Store.fromJson' => em chưa hiểu, nhờ thầy giải thích
  String? name;
  int? distance;

  Store(this.id, this.name, this.distance);

  Store.fromJson(Map<String, dynamic> jsonData) {
    this.id = int.parse(jsonData['id']);
    this.name = jsonData['name'];
    this.distance = jsonData['distance'];
  }
}
