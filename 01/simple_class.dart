import 'lib.dart';

void main() {
  Car car = Car();
  car.typeCart = "Xanh";
  print(car.typeCart);
  car.brand = "Brand 1";
  print(car.brand);

  Car car2 = Car.contructDemo(color: 'Xanh');
  print('Car 2: ${car2.brand}, ${car2.color}');

  Map<String, dynamic> dataJson = {'color': "đỏ", "brand": "OK"};
  Car car3 = Car.fromJson(dataJson);
  print('Car 3: ${car3.brand}, ${car2.color}');

  Car car4 = Car.create();
  print('Car 3: ${car4.brand}, ${car4.color}');
}
