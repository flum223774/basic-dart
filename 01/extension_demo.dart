import 'demo.dart';
/// [AnyThing] is a extension of List<init>
extension AnyThing on List<int> {
  /// [max] get max of list string
  int get max => reduce((value, element) {
    // compare value return and element
    if (value < element) {
      return element;
    } else {
      return value;
    }
  });

  int get min => reduce((value, element) {
    if (value > element) {
      return element;
    } else {
      return value;
    }
  });

  int other() {
    return findMin(this);
  }
}