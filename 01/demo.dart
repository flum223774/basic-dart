import 'dart:math';
import 'extension_demo.dart';

main() {
  // const Set<String> currencies = {'EUR', 'USD', 'JPY'};
  // print(currencies.join(','));

  // var | dynamic => compare with the first value it got
  // dynamic can get any value type
  dynamic a = 1;
  print(a);
  a = "haha";
  print(a);

  // final | const => cannot change value able
  // final can update value late
  // const must have value initializer when define
  final demoFinal = 100;
  // demoFinal = 10;
  // const int demoConst;
  print(demoFinal);

  List<int> arr = [6, 3, 4, 6, 3, 5, 7, 9, 1, 27, 5];
  // print(arr.reduce(max));
  print('Hello');
  print(findMin(arr));
  print(arr.min);
  print(arr.max);
  print(arr.other());
}

/// [numbers] prop list number to get min of the list
int findMin(List<int> numbers) {
  int min = 0;
  for (int i = 0; i < numbers.length; i++) {
    if (i == 0) {
      min = numbers[i];
      continue;
    }
    if (min > numbers[i]) {
      min = numbers[i];
    }
    if (i == 6) {
      break;
    }
    // print('Loop $i');
  }
  return min;
}
